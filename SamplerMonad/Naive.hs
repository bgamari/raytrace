{-# LANGUAGE CPP #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module SamplerMonad.Naive
    ( Gen
    , SamplerM, runSamplerM, liftST, liftRand
    , oneShotState
    ) where

import GHC.Magic (oneShot)
import Data.Word
import Control.Monad.ST
import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Class
import Control.Monad.State.Class

import Random

newtype SamplerM s a = SamplerM (StateT GenState (ST s) a)
  deriving (Functor, Applicative, Monad, MonadState GenState)

runSamplerM
    :: Word64 -> (forall s. SamplerM s a) -> a
runSamplerM seed sampler =
    runST $ case sampler of SamplerM action -> evalStateT action (mkGenState seed)

liftST :: ST s a -> SamplerM s a
liftST = SamplerM . lift
{-# INLINE liftST #-}

liftRand :: StateT GenState (ST s) a -> SamplerM s a
liftRand = SamplerM
{-# INLINE liftRand #-}

oneShotState :: SamplerM s a -> SamplerM s a
oneShotState (SamplerM action) = SamplerM $ StateT $ oneShot $ \s -> runStateT action s
{-# INLINE oneShotState #-}
