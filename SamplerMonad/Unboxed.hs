{-# OPTIONS_GHC -fobject-code #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE MagicHash #-}

module SamplerMonad.Unboxed
    ( Gen
    , SamplerM, runSamplerM, liftST, liftRand
    , oneShotState
    ) where

import System.Random.Stateful
import GHC.Magic (oneShot, runRW#)
import Control.Monad.ST
import Control.Monad.Trans.State.Strict

import GHC.ST hiding (liftST)
import GHC.Prim (State#)
import Control.Monad (ap)

type Gen = StdGen

data SamplerM s a = SamplerM (State# s -> StdGen -> (# State# s, StdGen, a #))

runSamplerM
    :: Gen -> (forall s. SamplerM s a) -> a
runSamplerM gen0 (SamplerM f) =
    runRW# (oneShot $ \s0 ->
      case f s0 gen0 of
        (# _s1, !_gen1, x #) -> x)

instance Functor (SamplerM s) where
  fmap f (SamplerM g) = SamplerM (oneShot $ \s0 -> oneShot $ \ !gen0 ->
    case g s0 gen0 of
      (# s1, gen1, x #) -> (# s1, gen1, f x #))

instance Applicative (SamplerM s) where
  pure x = SamplerM (oneShot $ \s0 -> oneShot $ \ !gen0 -> (# s0, gen0, x #))
  (<*>) = ap

instance Monad (SamplerM s) where
  return = pure
  SamplerM f >>= g = SamplerM (oneShot $ \s0 -> oneShot $ \ !gen0 ->
    case f s0 gen0 of
      (# s1, !gen1, x #) ->
        case g x of
          SamplerM h -> h s1 gen1)

liftST :: ST s a -> SamplerM s a
liftST (ST f) = SamplerM (oneShot $ \s0 -> oneShot $ \ !gen0 ->
    case f s0 of
      (# s1, x #) -> (# s1, gen0, x #))
{-# INLINE liftST #-}

liftRand :: StateT Gen (ST s) a -> SamplerM s a
liftRand (StateT f) = SamplerM (oneShot $ \s0 -> oneShot $ \ !gen0 ->
    case f gen0 of
      ST g ->
        case g s0 of
          (# s1, (x, !gen1) #) -> (# s1, gen1, x #))
{-# INLINE liftRand #-}

oneShotState :: SamplerM s a -> SamplerM s a
oneShotState (SamplerM action) =
    SamplerM $ oneShot $ \s0 -> oneShot $ \gen0 -> action s0 gen0
{-# INLINE oneShotState #-}
