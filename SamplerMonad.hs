{-# LANGUAGE CPP #-}

module SamplerMonad
    ( SamplerM, runSamplerM, liftST, liftRand
    , oneShotState
    ) where

#if defined(UNBOXED_SAMPLER)
import SamplerMonad.Unboxed
#else
import SamplerMonad.Naive
#endif
