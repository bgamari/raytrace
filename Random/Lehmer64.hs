{-# LANGUAGE CPP #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE MagicHash #-}
{-# OPTIONS_GHC -fobject-code #-}

module Random.Lehmer64 (Gen, gen, GenState, mkGenState) where

import System.Random.Stateful
import Control.Monad.Trans.State.Strict (State)
import Control.Monad.State.Class
import GHC.Word
import GHC.Prim (timesWord2#)

type Gen = Lehmer64Gen
type GenState = Lehmer64State

gen :: Gen
gen = Lehmer64Gen

mkGenState :: Word64 -> GenState
mkGenState x = Lehmer64State x x

data Lehmer64Gen = Lehmer64Gen

data Lehmer64State = Lehmer64State !Word64 !Word64

timesWord2 :: Word -> Word -> (Word, Word)
timesWord2 (W# a) (W# b) =
  case timesWord2# a b of (# x, y #) -> (W# x, W# y)

instance (Monad m, MonadState Lehmer64State m) => StatefulGen Lehmer64Gen m where
  {-# SPECIALISE instance StatefulGen Lehmer64Gen (State Lehmer64State) #-}
  uniformWord64 Lehmer64Gen = do
      Lehmer64State h l <- get
      let c = 0xda94042e4dd58b5
          (!x, !y) = timesWord2 (fromIntegral c) (fromIntegral l)
          l' = fromIntegral y
          h' = fromIntegral x + h * c
      put $! Lehmer64State h' l'
      return h'
  uniformShortByteString _ Lehmer64Gen = error "uniformShortByteString"
