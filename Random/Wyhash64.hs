{-# LANGUAGE CPP #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE MagicHash #-}
{-# OPTIONS_GHC -fobject-code #-}

module Random.Wyhash64 (Gen, gen, GenState, mkGenState) where

import System.Random.Stateful
import Control.Monad.Trans.State.Strict (State)
import Control.Monad.State.Class
import Data.Bits
import GHC.Word
import GHC.Exts

type Gen = Wyhash64Gen
type GenState = Wyhash64State

gen :: Gen
gen = Wyhash64Gen

mkGenState :: Word64 -> GenState
mkGenState x = Wyhash64State x

data Wyhash64Gen = Wyhash64Gen

data Wyhash64State = Wyhash64State !Word64

timesWord2 :: Word64 -> Word64 -> (Word64, Word64)
timesWord2 (W64# a) (W64# b) =
#if MIN_VERSION_base(4,15,0)
  case timesWord2# (word64ToWord# a) (word64ToWord# b) of
    (# x, y #) -> (W64# (wordToWord64# x), W64# (wordToWord64# y))
#else
  case timesWord2# a b of
    (# x, y #) -> (W64# $ wordToWord64# x, W64# $ wordToWord64# y)
#endif

instance (Monad m, MonadState Wyhash64State m) => StatefulGen Wyhash64Gen m where
  {-# SPECIALISE instance StatefulGen Wyhash64Gen (State Wyhash64State) #-}
  uniformWord64 Wyhash64Gen = do
      Wyhash64State x <- get
      put $! Wyhash64State (x + c1)
      let (tmp1_h, tmp1_l) = timesWord2 x c2
          m1 = tmp1_h `xor` tmp1_l :: Word64
          (tmp2_h, tmp2_l) = timesWord2 m1 c3
          m2 = tmp2_h `xor` tmp2_l
      return $! m2
    where
      c1 = 0x60bee2bee120fc15
      c2 = 0xa3b195354a39b70d
      c3 = 0x1b03738712fad5c9
  uniformShortByteString _ Wyhash64Gen = error "uniformShortByteString"

