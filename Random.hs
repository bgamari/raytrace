{-# LANGUAGE CPP #-}
{-# LANGUAGE BangPatterns #-}

module Random (Gen, gen, GenState, mkGenState) where

#if defined(RNG_LEHMER64)
import Random.Lehmer64
#elif defined(RNG_WYHASH64)
import Random.Wyhash64
#else

import System.Random.Stateful
import Data.Word

type Gen = StateGenM StdGen
type GenState = StdGen

gen :: Gen
gen = StateGenM

mkGenState :: Word64 -> GenState
mkGenState = mkStdGen . fromIntegral

#endif
