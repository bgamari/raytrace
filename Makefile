GHC ?= ghc
HCOPTS ?= -g3 -O2 -Wall -package random -package parallel \
					-ddump-to-file -dumpdir out -ddump-simpl -ddump-prep -dsuppress-ticks -dsuppress-coercions \
					-rtsopts -threaded

#TICKY=-ticky -ticky-allocd

all : Main

Main : Main.hs $(wildcard *.hs) cbits.c asmbits.o
	$(GHC) $< $(HCOPTS) $(EXTRA_HC_OPTS) $(TICKY) cbits.c asmbits.o

clean :
	rm -f *.o *.hi
