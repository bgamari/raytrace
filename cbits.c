#include <float.h>
#include <stdint.h>

double frobbleC(uint64_t x) {
  return (x >> 11) * DBL_EPSILON;
}
