{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Data.Semigroup
import Data.List.NonEmpty as NE (fromList)
import Control.Monad (replicateM)
import Control.Monad.Trans.State.Strict
import Control.Parallel.Strategies
import System.Random.Stateful

import qualified BVH
import qualified UnboxedBVH as UBVH
import Colour
import Figure
import Image
import Matrix
import Mesh
import RandomDist (choose)
import Sampler
import SamplerMonad
import Vector
import qualified STL

partitionPlane :: Coord -> Int -> [(Coord, Coord)]
partitionPlane size chunkSize =
    [ (lowerLeft, upperRight)
    | i <- [0 .. (coordX size + chunkSize - 1) `div` chunkSize - 1]
    , j <- [0 .. (coordY size + chunkSize - 1) `div` chunkSize - 1]
    , let lowerLeft = Coord (chunkSize * i) (chunkSize * j)
          upperRight = Coord (min (coordX size) (coordX lowerLeft + chunkSize - 1))
                             (min (coordY size) (coordY lowerLeft + chunkSize - 1))
    ]

main :: IO ()
main = do
    let nRuns = 2 :: Int
        chunkSize = 64 :: Int
        nSamples = 16 :: Int
        size = let n = 512 in Coord (2*n) n
        nRays = nRuns * nSamples * coordX size * coordY size

    rawFigures <- monkey
    -- let rawFigures = manySpheres
    putStrLn $ "Rays: " ++ show nRays
    putStrLn $ "Figures: " ++ show (length rawFigures)

    let !figure = sampleBVHFigure rawFigures
    --let figure = sconcat $ NE.fromList rawFigures

    putStrLn $ "Sampling..."
    -- let !sampler = sampleImagePixel size camera figure
    let !sampler = supersampleImagePixel nSamples size camera figure

    let singleImg :: Int -> [Image]
        singleImg seed =
          [ runSamplerM (fromIntegral seed) (generateImageM chunk sampler)
          | chunk <- imgChunks
          ]
        imgChunks :: [(Coord, Coord)]
        imgChunks = partitionPlane size chunkSize

    let !img = scaleImageValues (recip $ realToFrac nRuns)
              $ sumImages (Coord 0 0, size)
              $ withStrategy (parBuffer 1024 rseq)
              $ foldMap singleImg [1..nRuns]

    writePPMFile "out.ppm" $ gammaCorrect 2 img
    return ()

camera :: Camera
camera = mkCamera lookFrom lookAt viewUp fov aspect aperture
  where
    lookFrom = Pt $ Vec3 0 0 5
    lookAt   = Pt $ Vec3 0 0 (-1)
    viewUp   = Vec3 0 (-1) 0
    fov      = 45
    aspect   = 2
    aperture = PointAperture

sampleBVHFigure :: [Figure] -> Figure
sampleBVHFigure figures =
    UBVH.bvhFigure $ UBVH.packBVH $ runSamplerM 42 (BVH.mkBVH figures)
    --BVH.bvhFigure $ runSamplerM 42 (BVH.mkBVH figures)

meshToFigures :: Material -> Mesh.Mesh -> [Figure]
meshToFigures material = map convertTri . Mesh.meshTriangles
  where
    convertTri (Mesh.Triangle a b c) = triangle a b c material

monkey :: IO [Figure]
monkey = do
    mesh <- rotateMesh (rotationMat3 (Vec3 1 0 0) pi) <$> STL.readSTL "monkey.stl"
    mesh2 <- rotateMesh (rotationMat3 (Vec3 1 0 0) pi) <$> STL.readSTL "teapot.stl"
    let figs = concat
               [ meshToFigures material1 $ translateMesh (Vec3 (-2) 0 0) mesh
               , meshToFigures material2 $ translateMesh (Vec3 2    0 (-1)) mesh2
               , meshToFigures floorMaterial 
                 $ translateMesh (Vec3 0 1 (-1))
                 $ rotateMesh (rotationMat3 (Vec3 1 0 0) (pi/2))
                 $ xyQuadMesh (Vec2 5 5)
               , [sphere (Pt $ Vec3 0 0.5 2.5) 0.3 (dielectric 2.2)]
               , manySpheres
               ]
    return $ figs
  where
    material1 = withConstEmission (Colour 0.3 0 0) $ lambertian (Vec3 0.6 0.4 0.4)
    material2 = metal (Vec3 0.6 0.6 0.6) 0.2
    floorMaterial = metal (Vec3 0.3 0.3 0.4) 0.3
    grayMat n = lambertian (Vec3 n n n)

threeSpheres :: [Figure]
threeSpheres =
  [ sphere (Pt $ Vec3 0    0        (-1))   0.5             (lambertian $ Vec3 0.7 0 0)
  , sphere (Pt $ Vec3 1    0        (-1))   0.5             (metal (Vec3 0.8 0.6 0.2) 0.1)
  , sphere (Pt $ Vec3 (-1) 0        (-1))   0.5             (dielectric 2.2)
  , sphere (Pt $ Vec3 (-1) 1        (-1))   0.2             (withConstEmission (Colour 0.8 0.8 0.2) $ lambertian $ Vec3 0.8 0.8 0.2)
  , sphere (Pt $ Vec3 0    (-100.5) (-1))   100             (lambertian $ Vec3 0.2 0.2 0.8)
  ]

manySpheres :: [Figure]
manySpheres =
    spheres -- ++ [ floor ]
  where
    spheres =
      runStateGen_ (mkStdGen 42) $ replicateM 150 . sampleSphere

    floor =
      triangle p0 p1 p2
      $ metal (Vec3 0.7 0.7 0.7) 0.2
      where
        p0 = Pt $ Vec3 0     y 0
        p1 = Pt $ Vec3 10    y (-10)
        p2 = Pt $ Vec3 (-10) (y-1) (-10)
        y = (-3)

    sampleSphere :: RandomGen g => StateGenM g -> State g Figure
    sampleSphere gen = do
        pos <- Vec3 <$> d (-10,10) <*> d (-4,1) <*> d (0,-10)
        rad <- d (0.1, 0.5)
        colour <- Vec3 <$> d (0,1) <*> d (0,1) <*> d (0,1)
        material <- choose
          [ (1, return $ metal colour 0.4)
          , (0.3, return $ dielectric 2.0)
          ] StateGenM
        return $ sphere (Pt pos) rad material
      where
        d (a,b) = uniformRM (a,b) gen
